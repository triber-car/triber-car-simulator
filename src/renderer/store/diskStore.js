const Store = require('electron-store')
const store = new Store()

export function save (key, value) {
  return store.set(key, value)
}

export function load (key) {
  return store.get(key)
}
