import Vue from 'vue'
import Vuex from 'vuex'

import menu from './modules/Menu'
import settings from './modules/Settings'
import data from './modules/Data'
import stepper from './modules/Stepper'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    menu,
    settings,
    data,
    stepper
  },
  strict: true
})
