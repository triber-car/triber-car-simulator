import { PREVIOUS, NEXT } from './constants'

const initial = {
  step: 1
}

const mutations = {
  [NEXT]: (state) => {
    state.step++
  },
  [PREVIOUS]: (state, payload) => {
    state.step--
  }
}

const getters = {
  step: (state) => {
    return state.step
  }
}

export default {
  namespaced: true,
  state: initial,
  mutations,
  getters,
  strict: true
}
