import { UPDATE } from './constants'
import { save, load } from '../../diskStore'

const initial = () => {
  const init = load('settings')
  const empty = {
    host: null,
    port: null,
    apiKey: null
  }
  return init || empty
}

const mutations = {
  [UPDATE]: (state, payload) => {
    state.host = payload.host
    state.port = payload.port
    state.apiKey = payload.apiKey
    save('settings', payload)
  }
}

const getters = {
  host: (state) => {
    return state.host
  },
  port: (state) => {
    return state.port
  },
  apiKey: state => {
    return state.apiKey
  },
  valid: state => {
    return state.host && state.port && state.apiKey && /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i.test(state.apiKey)
  }
}

export default {
  state: initial,
  mutations,
  getters,
  strict: true
}
