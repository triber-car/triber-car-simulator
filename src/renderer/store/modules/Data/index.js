import { RESET_DATA, SET_FILENAMES, ADD_DATA, FILE_DONE } from './constants'

const initial = {
  data: {},
  files: []
}

const mutations = {
  [RESET_DATA]: (state) => {
    state.data = {}
    state.fileNames = []
    state.files = []
  },
  [SET_FILENAMES]: (state, payload) => {
    payload.forEach(element => {
      state.data[element.replace(/^.*[\\/]/, '')] = {file: element, data: [], numberOfEntries: 0}
      state.files.push({name: element.replace(/^.*[\\/]/, ''), done: false})
    })
  },
  [ADD_DATA]: (state, payload) => {
    state.data[payload.file].data.push(payload.data)
    state.data[payload.file].numberOfEntries++
  },
  [FILE_DONE]: (state, payload) => {
    state.files = state.files.map(it => it.name === payload.file.replace(/^.*[\\/]/, '') ? {...it, done: true} : it)
  }
}

const getters = {
  numberOfFiles: (state) => {
    return state.files.length
  },
  numberOfDone: (state) => {
    return state.files.filter(it => it.done).length
  },
  allDone: (state, getters) => {
    return getters.numberOfFiles === getters.numberOfDone
  }
}

export default {
  namespaced: true,
  state: initial,
  mutations,
  getters,
  strict: true
}
