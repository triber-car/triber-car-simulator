export const NAMESPACE = 'data'
export const RESET_DATA = 'resetData'
export const SET_FILENAMES = 'setFilenames'
export const ADD_DATA = 'addData'
export const FILE_DONE = 'fileDone'
