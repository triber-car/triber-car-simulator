import { TOGGLE } from './constants'

const initial = {
  show: false
}

const mutations = {
  [TOGGLE]: (state) => {
    state.show = !state.show
  }
}

const getters = {
  showMenu: (state) => {
    return state.show
  }
}

export default {
  state: initial,
  mutations,
  getters,
  strict: true
}
