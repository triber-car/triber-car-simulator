import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/home',
      name: 'home',
      component: require('@/pages/Home')
    },
    {
      path: '/settings',
      name: 'settings',
      component: require('@/pages/Settings')
    },
    {
      path: '*',
      redirect: '/home'
    }
  ]
})
