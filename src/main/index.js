'use strict'

import { app, BrowserWindow, Menu, ipcMain, dialog } from 'electron'
import fs from 'fs'
import readline from 'readline'
import { SIM_FILE_NAMES_EVENT, SIM_FILE_DATA_EVENT, SIM_FILE_DONE_EVENT } from '../communication/constants'

/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== 'development') {
  global.__static = require('path').join(__dirname, '/static').replace(/\\/g, '\\\\')
}

let mainWindow
const winURL = process.env.NODE_ENV === 'development'
  ? `http://localhost:9080`
  : `file://${__dirname}/index.html`

function createWindow () {
  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    height: 563,
    useContentSize: true,
    width: 1000,
    webPreferences: {
      nodeIntegrationInWorker: true
    }
  })

  mainWindow.loadURL(winURL)

  mainWindow.on('closed', () => {
    mainWindow = null
  })

  var template = [{
    label: 'Application',
    submenu: [
      { label: 'About Application', selector: 'orderFrontStandardAboutPanel:' },
      { type: 'separator' },
      { label: 'Quit', accelerator: 'Command+Q', click: function () { app.quit() } }
    ]}, {
    label: 'Edit',
    submenu: [
      { label: 'Undo', accelerator: 'CmdOrCtrl+Z', selector: 'undo:' },
      { label: 'Redo', accelerator: 'Shift+CmdOrCtrl+Z', selector: 'redo:' },
      { type: 'separator' },
      { label: 'Cut', accelerator: 'CmdOrCtrl+X', selector: 'cut:' },
      { label: 'Copy', accelerator: 'CmdOrCtrl+C', selector: 'copy:' },
      { label: 'Paste', accelerator: 'CmdOrCtrl+V', selector: 'paste:' },
      { label: 'Select All', accelerator: 'CmdOrCtrl+A', selector: 'selectAll:' }
    ]}
  ]

  Menu.setApplicationMenu(Menu.buildFromTemplate(template))
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})

app.setAboutPanelOptions({
  applicationName: 'Triber-car-simulator',
  applicationVersion: 'Tha latest',
  version: 'Also tha latest',
  copyright: 'Made by triberraar',
  credits: 'me aka triberraar'
})

ipcMain.on('chooseSimfile', (event, path) => {
  dialog.showOpenDialog({properties: ['openFile', 'multiSelections']}, function (fileNames) {
    if (fileNames) {
      event.sender.send(SIM_FILE_NAMES_EVENT, fileNames)
      fileNames.forEach(it => {
        readFile(event.sender, it)
      })
    }
  })

  function readFile (sender, filepath) {
    var rl = readline.createInterface({
      input: fs.createReadStream(filepath)
    })

    rl.on('line', (line) => {
      try {
        const inp = line.replace(/;+$/g, '')
        if (inp) {
          const val = JSON.parse(line.replace(/;+$/g, ''))
          sender.send(SIM_FILE_DATA_EVENT, {file: filepath, data: val})
        }
      } catch (e) {
        console.log(`something went wrong when parsing: ${e}, skipping`)
      }
    })

    rl.on('close', () => {
      event.sender.send(SIM_FILE_DONE_EVENT, {file: filepath})
    })
  }
})

/**
 * Auto Updater
 *
 * Uncomment the following code below and install `electron-updater` to
 * support auto updating. Code Signing with a valid certificate is required.
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-electron-builder.html#auto-updating
 */

/*
import { autoUpdater } from 'electron-updater'

autoUpdater.on('update-downloaded', () => {
  autoUpdater.quitAndInstall()
})

app.on('ready', () => {
  if (process.env.NODE_ENV === 'production') autoUpdater.checkForUpdates()
})
 */
